<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<body>
<br><br>
<h1><center>${msg}</center></h1>
<h2><center>Today is <fmt:formatDate value="${today}" pattern="yyy-MM-dd" /></center></h2>
</body>
</html>
